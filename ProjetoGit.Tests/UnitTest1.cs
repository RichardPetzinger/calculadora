using NUnit.Framework;

namespace ProjetoGit.Tests
{
    public class Tests
    {
        [Test]
        public void TesteDeSoma()
        {
            int n1 = 3,
                n2 = 1,
                result = 4;
            Assert.AreEqual(result, Calculadora.Soma(n1,n2));
        }
        [Test]
        public void TesteDeSubtracao()
        {
            int n1 = 6,
                n2 = 3,
                result = 3;
            Assert.AreEqual(result, Calculadora.Subtracao(n1,n2));
        }
        [Test]
        public void TesteDeMultiplicacao()
        {
            int n1 = 3,
                n2 = 2,
                result = 6;
            Assert.AreEqual(result, Calculadora.Multiplicacao(n1,n2));
        }
        [Test]
        public void TesteDeDivisao()
        {
            int n1 = 14,
                n2 = 2,
                result = 7;
            Assert.AreEqual(result, Calculadora.Divisao(n1,n2));
        }
    }
}