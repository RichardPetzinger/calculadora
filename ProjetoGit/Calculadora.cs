namespace ProjetoGit
{
    public class Calculadora
    {
        public static int Soma(int numero1, int numero2)
        {
            return numero1 + numero2;
        }
        public static int Subtracao(int numero1, int numero2)
        {
            return numero1 - numero2;
        }
        public static int Multiplicacao(int numero1, int numero2)
        {
            return numero1 * numero2;
        }
        public static int Divisao(int numero1, int numero2)
        {
            int resultado = (numero2 == 0) ? 0 : numero1 / numero2;
            return resultado;
        }
    }
}